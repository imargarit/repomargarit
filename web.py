from flask import Flask, jsonify,request,render_template
from flask_cors import CORS,cross_origin
from flask_restful import Resource, Api
import requests
from werkzeug import secure_filename
from PIL import Image, ImageFilter
import json
from PIL import Image, ImageDraw, ImageFont
import tensorflow as tf
import qrcode
import qrcode.image.svg

app = Flask(__name__)
CORS(app, support_credentions=True)

def getPrediction(target_file):
    cv_headers = {'Prediction-Key': "931df28e11cb4bad985c9f15c97564b0", 'Content-Type': "application/json"}
    file = target_file
    image_send = open(file, "rb").read()
    URL = "https://northeurope.api.cognitive.microsoft.com/customvision/v3.0/Prediction/6666c0cf-c7a5-4449-8bb9-5f337891c21a/classify/iterations/Iteration11/image"
    cv_response = requests.post(URL, headers=cv_headers, data=image_send)
    cv_response.raise_for_status()
    prediction = cv_response.json()
    prediction = prediction['predictions'][0]['tagName']
    return prediction


@app.route('/')
def home():
    return render_template('VAMA.html')


@app.route('/uploader',methods=['GET','POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        fileName = secure_filename(f.filename)
        f.save(fileName)
         #apelezi predictia cu fileName
        return render_template('raspuns.html', result=str(getPrediction(fileName)))

#@app.route('/ticket', methods=['POST'])
#def qr_code():
    #method = "basic"

    #data = "CE FACI TUUUUUU??????"

    #if method == 'basic':
        factory = qrcode.image.svg.SvgImage
    #elif method == 'fragment':
        #factory = qrcode.image.svg.SvgFragmentImage
    #elif method == 'path':
        # Combined path factory, fixes white space that may occur when zooming
        #factory = qrcode.image.svg.SvgPathImage
        # Set data to qrcode
        #img = qrcode.make(data, image_factory=factory)
        # Save svg file somewhere
        #return render_template('vama.html',result=img.save("papa.svg"))

if __name__ == '__main__':
    app.run(debug=True)


